# 1. Installation

## Create new python environment
```bash
conda create -n benchmarking python=3.10
conda activate benchmarking
```

## Install dependencies
```bash
pip install --upgrade numpy batchgenerators
pip install torch
```


# 2. Run instructions
`python run_benchmark.py -o OUTPUT_FILE.json`

IGNORE ANY ERROR MESSAGES THAT CAN APPEAR AT THE END. If the file is generated then the run was successful ;-)

# 3. Result interpretation
json file will contain the following keys:
- "cpu_benchmark" = "How quickly can I run data augmentation?". Measured in number of batches per second. Faster is better.
- "ppc_benchmark" = "How quickly can batches be transferred between background workers and the main process?"
- "lscpu" give the lscpu output (for remembering what CPU was used)
- "hostname" system hostname
- "kernel" linux kernel version

## cpu_benchmark
We test the throughput at different numbers of background processes. This is to measure single threaded performance, 
multiprocessing performance as well as assess CPU performance over varying loads. Modern processors adjust their 
frequency depending on power consumption  TDP, temperature etc, so this provides useful data points for comparing 
systems. Some scale better than others!

The "cpu_benchmark" key in the results file is a dictionary with its keys being the number of processes used and the 
values being the results.

CPU benchmark scales the workload with the number of processes used. So 2 processes get 2x the work that 1 process 
gets. This is to keep the run times of each experiment somewhat constant.

Example:
```
    "cpu_benchmark": {
        "1": {
            "total_time": 25.87,
            "num_batches_generated": 30,
            "time_per_batch": 0.8623333333333334,
            "throughput": 1.1596443757247776
        },
        ...
        "16": {
            "total_time": 49.49,
            "num_batches_generated": 480,
            "time_per_batch": 0.10310416666666668,
            "throughput": 9.698929076581127,
            "speedup": 8.363709840371792,
            "scaling": 0.522731865023237
        }
```
This particular system had a throughput 1.6 batches/s with 1 Process and 9.70 with 16. Given that this is an 8C/16T 
part, a speedup of 8.36 is what one would expect. Speedup is always measured relative to single process throughput! 
Remember that frequency goes down the more cores are loaded!

Details:
- "total_time": time required to complete the workload
- "num_batches_generated": how many batches were generated in this workload?
- "time_per_batch": total_time / num_batches_generated
- "throughput": how many batches are generated per second?
- "speedup": how much faster was this run compared to using a single process? -> throughput@here/throughput@1P
- "scaling": given that I now use XP instead of 1P, how much do we deviate from perfect scaling? 1=perfect smaller 
is worse. Should remain high until we hit hyperthreading!

## ppc_benchmark
How quickly can we transfer data between processes? This benchmark measures the speed of the memory subsystem + CPU. 
In a worst case scenario, we an experiment could produce and consume batches faster than they can be transferred. 
Not highly relevant because this is rarely/never the bottleneck but still interesting!

Results here are just a list testing different configurations from smaller to larger data transfers. I think only the 
largest one at the bottom is really relevant:

```
        {
            "batch_size": 16,
            "img_size": [
                48,
                224,
                224
            ],
            "speed_npy": 6.03,
            "speed_torch_tensor": 0.69
        }
```
Look at the "speed_npy" and "speed_torch_tensor" results. **Lower is better** (speed is given in s).
