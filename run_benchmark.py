
import numpy as np
from time import time
import multiprocessing

from batchgenerators.dataloading.data_loader import SlimDataLoaderBase
from batchgenerators.dataloading.multi_threaded_augmenter import MultiThreadedAugmenter
from batchgenerators.transforms.abstract_transforms import Compose
from batchgenerators.transforms.color_transforms import GammaTransform, BrightnessTransform
from batchgenerators.transforms.noise_transforms import GaussianNoiseTransform, GaussianBlurTransform
from batchgenerators.transforms.spatial_transforms import MirrorTransform, SpatialTransform
from batchgenerators.transforms.utility_transforms import NumpyToTensor
from batchgenerators.utilities.file_and_folder_operations import *


class DataLoader(SlimDataLoaderBase):
    def __init__(self, batch_size, image_size=(3, 224, 224)):
        super(DataLoader, self).__init__(None, batch_size)
        self._data = np.random.random((batch_size, *image_size)).astype(np.float32)

    def generate_train_batch(self):
        return {'data': self._data}


def get_pipeline():
    transforms = []
    transforms.append(MirrorTransform((0, 1)))
    transforms.append(
        SpatialTransform((224, 224), (112, 112), True, do_rotation=True, do_scale=True, random_crop=False))
    transforms.append(
        SpatialTransform((224, 224), (112, 112), True, do_rotation=True, do_scale=True, random_crop=False))
    transforms.append(
        SpatialTransform((224, 224), (112, 112), True, do_rotation=True, do_scale=True, random_crop=False))
    transforms.append(GaussianNoiseTransform())
    transforms.append(GaussianBlurTransform())
    transforms.append(GammaTransform())
    transforms.append(BrightnessTransform(0, 0.1))
    transforms.append(NumpyToTensor('data'))
    transform = Compose(transforms)
    return transform


def time_it(dl, num_batches=100, num_batches_warmup=20):
    for _ in range(num_batches_warmup):
        _ = next(dl)
    st = time()
    for _ in range(num_batches):
        _ = next(dl)
    end = time()
    # print(end - st, "seconds")
    return np.round(end - st, decimals=2)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', type=str, required=False, default="benchmark_batchgeneratorsv2.json", help='output file')
    args = parser.parse_args()
    multiprocessing.set_start_method("spawn")

    print("each batch has size (batch_size, 3, 224, 224)")
    batch_size = 16
    num_batches_benchmark = 30
    num_batches_warmup = 4
    print("batch size is", batch_size)

    num_cpus = multiprocessing.cpu_count()
    test_these_num_processes = np.floor(np.log(num_cpus) / np.log(2)).astype(int)
    test_these_num_processes = [2 ** i for i in range(test_these_num_processes + 1)]
    if test_these_num_processes[-1] != num_cpus:
        test_these_num_processes.append(num_cpus)

    if num_cpus // 2 not in test_these_num_processes:
        test_these_num_processes.append(num_cpus // 2)
        test_these_num_processes.sort()

    results_dl = {}
    for num_processes in test_these_num_processes:
        dl = DataLoader(batch_size)
        pipeline = get_pipeline()
        mtgen = MultiThreadedAugmenter(dl, pipeline, num_processes, 3, None)
        mtgen.restart()
        time_multi_threaded = time_it(mtgen, num_processes * num_batches_benchmark, num_processes * num_batches_warmup)
        mtgen._finish()
        results_dl[num_processes] = {}
        results_dl[num_processes]['total_time'] = time_multi_threaded
        results_dl[num_processes]['num_batches_generated'] = num_processes * num_batches_benchmark
        results_dl[num_processes]['time_per_batch'] = time_multi_threaded / (num_processes * num_batches_benchmark)
        results_dl[num_processes]['throughput'] = (num_processes * num_batches_benchmark) / time_multi_threaded
        if num_processes > 1:
            results_dl[num_processes]['speedup'] = results_dl[1]['total_time'] / time_multi_threaded * num_processes
            results_dl[num_processes]['scaling'] = results_dl[1]['total_time'] / time_multi_threaded

    results_proc_comm = []
    print("Now we test process-process communication overhead")
    for i, (batch_size, img_size) in enumerate(zip((2, 16, 16, 16, 16), ((3, 224, 224), (3, 224, 224),
                                                                         (12, 224, 224), (48, 224, 224),
                                                                        ))): # (48 * 4, 224, 224)
        print("data size:", str((batch_size, *img_size)))

        dl = DataLoader(batch_size, img_size)
        mtgen = MultiThreadedAugmenter(dl, None, 4, num_cached_per_queue=3, seeds=None)
        mtgen.restart()
        np_arr = time_it(mtgen, 48, 8)

        dl = DataLoader(batch_size, img_size)
        mtgen = MultiThreadedAugmenter(dl, NumpyToTensor(), 4, num_cached_per_queue=3, seeds=None)
        mtgen.restart()
        np_tor = time_it(mtgen, 48, 8)

        results_proc_comm.append({'batch_size': batch_size,
                                'img_size': img_size,
                                'speed_npy': np_arr,
                                'speed_torch_tensor': np_tor,
                                })

    import subprocess
    lscpu = subprocess.getoutput('lscpu')
    hostname = subprocess.getoutput('hostname')

    write_json({
        'cpu_benchmark': results_dl,
        'ppc_benchmark': results_proc_comm,
        'lscpu': lscpu,
        'hostname': hostname,
        'kernel': subprocess.getoutput('uname -r')
    }, args.o, sort_keys=False)

